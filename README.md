# CODENOTE

Neatly packaged interactive JavaScript notebooks
[![npm](https://img.shields.io/npm/v/codenote.svg?maxAge=2592000)](https://www.npmjs.com/package/codenote)

See the Introduction to CodeNote [here](http://petroff.ryan.gitlab.io/codenote/) (Find these files hosted from the gitlab-io-hosted branch)

## Usage  
Codenote is a tool designed for the command line, so let's start there. To install codenote:  
`npm install --global codenote`  
CodeNote is installed!  

Let's use it. We need a markdown file to target, so we can make one.  
`codenote md filename`  

This creates filename.md.    
With our .md file in hand, we do some writing.    
Write, write, write.   
Eventually, we're ready to share.    
To turn our simple .md file into a glorious CodeNote .html:   
`codenote html myfile.md`   
This will generate our .html file, ready for static publishing.   
Load it up in your browser! \^\_\^   

---
Past this point it's a little speculative, the code may not exist yet.
---


The most complex mode is LIVE mode.
Invoke live mode thusly:
```BASH
codenote live yourfile.md
```
This will put CodeNote into LIVE mode.
CodeNote will generate a yourfile.html file from yourfile.md,
and load yourfile.html in your browser, and place a live watch on yourfile.md for changes.
This means if you make changes and save them to yourfile.md while LIVE mode is active,
CodeNote will see that yourfile.md changed, rebuild a fresh .html, and reload the browser.

The final mode for CodeNote is not implemented, but it's codenote publish :)



This repo has not fully removed the kajero stuff, but I want to get the namespace now.
I'll put this up now and clean it up as we go along.
Nothing here isn't already public - it's here because I cloned it from Kajero!
Kajero means 'notebook' in Esperanto :)

[![npm](https://img.shields.io/npm/v/codenote.svg?maxAge=2592000)](https://www.npmjs.com/package/codenote)
Maybe I should have a gitter too? People can just email me...

Right now it's supposed to be like:
codenote html ./sdf.md        (cmd.js)
and it generates the .html
in the folder you called codenote in, pwd.



## FORKED FROM Kajero

You can view a sample notebook [here](http://www.joelotter.com/kajero).

![](https://raw.githubusercontent.com/JoelOtter/kajero/master/doc/screenshot.png)


### Installation

`npm install -g kajero`, or clone this repository.

You can build the JS library by running `npm install`, followed by `gulp`. For a production build, `NODE_ENV=production gulp`.
